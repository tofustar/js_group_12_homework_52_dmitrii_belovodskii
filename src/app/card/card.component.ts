import {Component, Input} from '@angular/core';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.css']
})
export class CardComponent {
  @Input() rank = '';
  @Input() suit = '';

  getRank() {
    switch (this.rank) {
      case '2':
        return '2';
      case '3':
        return '3';
      case '4':
        return '4';
      case '5':
        return '5';
      case '6':
        return '6';
      case '7':
        return '7';
      case '8':
        return '8';
      case '9':
        return '9';
      case '10':
        return '10';
      case 'j':
        return 'J';
      case 'q':
        return 'Q';
      case 'k':
        return 'K';
      case 'a':
        return 'A';
      default:
        return '';
    }
  }

  getSuit() {
    switch (this.suit) {
      case 'diams':
        return '♦';
      case 'hearts':
        return '♥';
      case 'clubs':
        return '♣';
      case 'spades':
        return '♠';
      default:
        return '';
    }
  }

  getClassname() {
    return `card rank-${this.rank} ${this.suit}`
  }

}
