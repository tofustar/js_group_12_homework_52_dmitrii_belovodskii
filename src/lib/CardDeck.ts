const ranks = ['2', '3', '4', '5', '6', '7', '8', '9', '10', 'J', 'Q', 'K', 'A'];
const suits = ['diams', 'hearts', 'clubs', 'spades'];

export class Card{
  rank= '';
  suit = '';
  constructor(rank:string, suit:string) {
    this.rank = rank;
    this.suit = suit;
  }

  getScore(){
    let parseRank = parseInt(this.rank);
    if (this.rank === 'A') return 11;
    if (this.rank === 'J' || 'Q' || 'K') return 10;
    if (parseRank) return parseRank;
    else return '';
  }
}
export class CardDeck{
  cards: Card[] = [];

  constructor() {
    for (let i = 0; i < ranks.length; i++){
      for(let j = 0; j < suits.length; j++) {
        const newCard = new Card(ranks[i], suits[j]);
        this.cards.push(newCard);
      }
    }
  }

  getCard() {
    const getRandomNumber = Math.floor(Math.random() * this.cards.length);
    const deletedCard = this.cards.splice( getRandomNumber, 1);
    return deletedCard[0];
  }

  getCards(howMany: number) {
    const newCardsArray = [];
    for(let i = 0; i < howMany; i++) {
      newCardsArray.push(this.getCard());
    }
    return newCardsArray;
  }
}
